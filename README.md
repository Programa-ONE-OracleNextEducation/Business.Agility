# **Business Agility**

## **Empresa Ágil**

Estamos viviendo en un mundo donde las empresas necesitan reinventarse a cada día. Nuevas formas de trabajar son creadas, nuevos modelos de negocios surgen, nuevos desafíos aparecen en el día a día de las empresas.
¿Que es business agility?

Business Agility (agilidad en los negocios, traducción libre) es la capacidad que las empresas tienen para poder adaptarse rápidamente a los cambios, entregando valor para sus clientes.
¿Por qué Business Agility es importante?

En la formación de Business Agility vas a poder aprender como tu empresa puede prepararse para un escenario de incertidumbre y cambios constantes, entender cuál es el Mindset Ágil que las empresas necesitan, como estructurar equipos ágiles y como medir resultados tanto para el negocio como para la operación.

Cuando se comienza a ver algunos resultados fruto de las implementaciones Agile, es necesario formar nuevos Agile Coaches para que puedan expandir esa cultura de agilidad, entrenar nuevos equipos y así escalar el ambiente ágil.

Este puede ser el primer paso para una transformación disruptiva en tu organización. ¿Qué dices?, ¿comenzamos?.

Esta formación forma parte del Programa ONE, una alianza entre Alura Latam y Oracle.

## **De quien vas a aprender**

- Elizabeth Leon Garduno
- Jimena Alzamora

## **Paso a paso**

### **1. Fundamentos**

En este primer bloque vas a aprender lo que realmente es necesario para que tus iniciativas ágiles tengan éxito. Es el punto de partida de cualquier profesional, donde vas a aprender desde el manifiesto ágil, que es de donde surgió el método ágil, flujos y casos de éxito.

Todos estos aprendizajes van a servir de base para la adaptación de agilidad en un ambiente corporativo.

[Fundamentos de Agilidad: Primeros pasos para la transformación ágil](https://app.aluracursos.com/course/fundamentos-agilidad-primeros-pasos-transformacion-agil "Fundamentos de Agilidad: Primeros pasos para la transformación ágil"). Contenido [aqui](./Docs/fundamentos_de_agilidad.md).

[¿Qué es la metodologia Ágil?](https://www.aluracursos.com/blog/que-es-la-metodologia-agil "Alura LATAM").

### **2. El ambiente ágil**

¿Cuál será la cultura necesaria para tener un ambiente ágil?

En este momento vamos a explorar un poco el Mindset necesario para tener una organización que realmente puede embarcar en ese mundo ágil. Vamos a explorar características de autonomía, confianza y colaboración que son indispensables en este proceso.

[Gestión ágil: Liderando el cambio en un ambiente de agilidad](https://app.aluracursos.com/course/gestion-agil-liderando-cambio "Gestión ágil: Liderando el cambio en un ambiente de agilidad"). Contenido [aqui](./Docs/gestion_agil.md).

[La Empresa Ágil: Introduciendo el Business Agility en las organizaciones](https://app.aluracursos.com/course/empresa-agil-introduciendo-business-agility-organizaciones "La Empresa Ágil: Introduciendo el Business Agility en las organizaciones"). Contenido [aqui](./Docs/la_empresa_agil.md).

[¿Qué es Management 3.0 y por qué aplicarlo en su empresa?](https://www.aluracursos.com/blog/que-es-management-3-0-y-por-que-aplicarlo-en-tu-empresa "Alura LATAM").

### **3. Organizando equipos ágiles**

Para que una transformación ágil sea bien exitosa, es necesario organizarse y definir los papeles y responsabilidades de cada uno de los actores dentro de un contexto de ambiente ágil.

En este bloque vamos a explorar como los equipos ágiles suelen organizarse, cuáles son los papeles y personas necesarias y además vamos a aprender como llevar esa transformación para otras áreas de la empresa.

Al final, vamos a explorar el papel del principal facilitador en este proceso, el Agile Coach y de qué manera este contribuye para el éxito de la adopción ágil.

[Quiero trabajar en una startup. ¿Qué necesito hacer?](https://www.aluracursos.com/blog/quiero-trabajar-en-una-startup-que-necesito-hacer "Alura LATAM").

[Organización de equipos ágiles: Las funciones existentes en un equipo](https://app.aluracursos.com/course/organizacion-equipos-agiles-funciones-existentes-equipo "Organización de equipos ágiles: Las funciones existentes en un equipo"). Contenido [aqui](./Docs/organizacion_de_equipos_agiles.md).

[Cómo implementar una cultura de aprendizaje en tu empresa](https://www.aluracursos.com/blog/como-implementar-una-cultura-de-aprendizaje-en-tu-empresa "Alura LATAM").

### **4. Diagnóstico de lo aprendido**

Este paso es obligatorio Cuéntanos sobre tus conocimientos adquiridos hasta el momento. Es un diagnóstico personal y por ello es muy importante que seas sincero con tus respuestas.

[Ruta B. Agility](https://grupocaelum.typeform.com/to/SO0Z4cId "Ruta B. Agility").
