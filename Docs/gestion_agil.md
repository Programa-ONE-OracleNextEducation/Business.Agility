## **Realice este curso para Transformación Ágil y:**

- Entienda la diferencia entre la Gestión Tradicional y la Gestión Ágil
- Conozca el concepto VUCA (Volatility, Uncertainty, Complexity, Ambiguity)
- Aprenda por qué Motivación y Liderazgo son esenciales en la Gestión Ágil
- Sepa de la importancia de la mejora continua para un proceso ágil

### **Aulas**

- **Gestión Tradicional**

  - Presentación
  - Gestión Tradicional
  - Pensadores y ejecutores
  - Gestión dentro de tu contexto
  - Para saber más
  - Cómo mejorar la gestión a diario - Parte 1
  - Cómo mejorar la gestión a diario - Parte 2
  - Lo que aprendimos

- **VUCA**

  - Mundo VUCA
  - Ambigüedad
  - Volatilidad en las organizaciones
  - Lo que aprendimos

- **Motivación**

  - Motivación
  - El mayor valor del mundo VUCA
  - Para saber más: Personal maps
  - Confianza dentro del mundo corporativo
  - Lo que aprendimos

- **Liderazgo**

  - Liderazgo
  - Heteronomía
  - Para saber más: Autonomía en Spotify
  - Liderazgo en tu organización
  - Lo que aprendimos

- **Empoderar y delegar**

  - Empoderar y delegar
  - Los 7 niveles del Delegation Poker
  - Mapeando el proceso de decisión
  - Lo que aprendimos

- **Retroalimentación y conocimiento**

  - Retroalimentación
  - Retroalimentación
  - Dando feedback positivo
  - Conocimiento
  - Lo que aprendimos

- **Mejoras**

  - Mejoras
  - Kaizen y Kaikaku
  - Mejora continua de procesos
  - Scrum y Kanban
  - Lo que aprendimos
  - Conclusión
