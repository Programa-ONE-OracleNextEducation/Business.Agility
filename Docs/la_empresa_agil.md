## **Realice este curso para Transformación Ágil y:**

- Aprenda como una empresa apende a ser Ágil
- Descubra como trabajar con Bussiness Agility
- Entienda la importancia de foca en generación de valor
- Conozca como funciona la estrutura organizacional de una empresa ágil

### **Aulas**

- **Business Agility**

  - Presentación
  - Business Agility
  - Porque Business Agility
  - Diseñemos la Agilidad empresarial
  - Propósito del Business Agility
  - Lo que aprendimos

- **Identificando la complejidad**

  - Identificando la complejidad
  - Problemas subjetivos
  - Identificando complejidades
  - Lo que aprendimos

- **Estructura organizacional**

  - Estructura organizacional
  - Estructura Jerárquica
  - La Mejor Estructura Organizacional
  - Reflexione sobre su estructura
  - Lo que aprendimos

- **El Rayo X de la empresa ágil**

  - El Rayo X de la empresa ágil
  - Construyendo un escenario más ágil
  - Reduciendo Silos
  - Lo que aprendimos

- **Enfoque en la generación de valor**

  - Enfoque en la generación de valor
  - La Menor Unidad de Valor
  - Encontrando tus unidades de valor
  - Lo que aprendimos

- **Como una empresa aprende a ser ágil**

  - Como una empresa aprende a ser ágil
  - Procesos para tener una empresa ágil
  - ¿Cómo tu empresa puede ser ágil?
  - Lo que aprendimos
  - Conclusión
