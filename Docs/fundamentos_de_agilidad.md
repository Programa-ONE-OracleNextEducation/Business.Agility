## **Realice este curso para Transformación Ágil y:**

- Entienda porque surgió el Método Ágil
- Descubra las diferencias entre el Método Ágil y el Waterfall
- Aprenda sobre la priorización de tareas y flujos
- Sepa porque el feedback debe ser el más rápido posible
- Conozca el Manifesto Ágil

### **Aulas**

- **El Método Ágil (Agile)**

  - Presentación
  - Método Ágil (Agile)
  - Origen del método ágil
  - Lo que aprendimos en aula

- **Método Cascada (Waterfall)**

  - Método Cascada (Waterfall)
  - Características de Waterfall
  - Lo que aprendimos en aula

- **Priorización**

  - Priorización
  - Cómo priorizar
  - Lo que aprendimos en aula

- **Flujo de ejecución**

  - Flujo de ejecución
  - Agile y Waterfall
  - Lo que aprendimos en aula

- **Retroalimentación**

  - Retroalimentación
  - Retroalimentación
  - Lo que aprendimos en aula

- **¿Qué es ser ágil?**

  - Qué es ser ágil
  - ¿Por qué es importante ser ágil?
  - Lo que aprendimos en aula

- **Manifiesto ágil**

  - Manifiesto ágil
  - La relevancia del Manifiesto Ágil
  - Lo que aprendimos en aula

- **Cases**

  - Cases
  - Agile en tu compañía
  - Lo que aprendimos en aula
  - Conclusión
