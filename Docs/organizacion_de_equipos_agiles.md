## **Realice este curso para Transformación Ágil y:**

- Entenda como trabaja un equipo ágil
- Descubra como son formados los diferentes tipos de equipos ágiles
- Aprenda la división de tareas dentro del equipo
- Conozca cuales son las responsabilidades del Product Owner y del Scrum Master

### **Aulas**

- **El equipo ágil**

  - Presentación
  - El equipo ágil
  - Formando un nuevo equipo
  - Equipos Ágiles
  - Lo que aprendimos

- **La autoorganización**

  - Autoorganización
  - Autoorganización
  - Auto-Organización en la práctica
  - Lo que aprendimos

- **Los tipo de equipo ágil**

  - Tipos de equipos
  - Los tipos de equipos ágiles
  - Equipos en ambientes ágiles
  - Lo que aprendimos

- **El equipo Scrum**

  - Scrum Team
  - El scrum team
  - Organizando equipos
  - Lo que aprendimos

- **El Product Owner**

  - El product owner
  - El rol del Product Owner
  - ¿Qué es el Product Owner en el marco de Scrum y cuáles son sus áreas de atención?
  - Lo que aprendimos

- **El Development Team**

  - Development Team
  - El rol del development team
  - Development Team fuera de tecnología
  - Lo que aprendimos

- **El Scrum Master**

  - Scrum Master
  - Características del Scrum Master
  - Preguntas para reflexionar
  - Lo que aprendimos

- **Los Equipos Kanban**

  - Equipos Kanban
  - Yendo a fondo en Kanban
  - Profundizando Scrum
  - Lo que aprendimos
  - Conclusión
