# **Gestión ágil - Liderando el cambio en un ambiente de agilidad**

## **Gestión tradicional**

### **Pensadores y ejecutores**

En un ambiente de gestión tradicional, ¿quién suele recibir retroalimentación por parte del cliente?

Rta.

Pensador. Este, a su vez, toma decisiones basadas en la retroalimentación con un bajo nivel de transparencia.

### **Para saber más**

Este artículo de la revista [Forbes](https://www.forbes.com/sites/worldeconomicforum/2016/02/03/these-3-management-styles-belong-in-the-past/?sh=4cc23a102a18 "Forbes") (en inglés) muestra un resumen de las características que diversos estudios muestran al respecto de lo que está cambiando en los tipos de liderazgo y autonomía, el famoso concepto "mesa de niños", gestión top-down etc. Esta lectura sirve como excelente complemento para esta lección y las próximas de este curso.

En caso de que no domines el inglés, puedes usar el traductor de tu navegador.

### **Lo que aprendimos**

La gestión tradicional puede haber funcionado durante mucho tiempo, pero hoy comienza a dar señales de que está perdiendo espacio, ya que está relacionada con un ambiente de baja autonomía, bajo nivel de engagement y poca motivación, que tienen un impacto directo en el nivel de éxito de los proyectos.

---

## **VUCA**

### **Ambigüedad**

¿Cuál es la definición más adecuada de ambigüedad dentro de una organización?

Rta.

Las soluciones correctas para un problema pueden ser las menos indicadas para otro similar en un contexto diferente. La ambigüedad se refiere al hecho de que aquellas soluciones para una determinada situación pueden ser las menos indicadas para otras semejantes en otro contexto.

### **Volatilidad en las organizaciones**

Dentro de tu experiencia profesional, ¿hubo algún proceso que cambió completamente debido a una nueva tecnología (sea software o hardware) y requirió que te adaptaras al cambio?

Un ejemplo práctico son las máquinas de reloj checador (time clock o clock card en inglés). En el pasado, las organizaciones tenían cuadernos donde registraban las horas o días laborales de sus trabajadores. Posteriormente, comenzaron las tarjetas, donde era más fácil llevar este control. Hoy en día, hay empresas con un modelo de home office, en donde no es necesario hacer este registro. Además, hay organizaciones que permiten la entrada a sus instalaciones a través de una aplicación. Reflexiona cómo ha cambiado este proceso a lo largo del tiempo y ha evolucionado junto con el mercado tecnológico.

### **Lo que aprendimos**

Nuestro entorno cada vez tiene más características VUCA:

- Volátil: algo que hoy se da por hecho puede ser diferente mañana.
- Incierto: hay muchas dudas para lograr resolver los problemas.
- Complejo: la solución no tiene necesariamente una estructura clara.
- Ambiguo: lo que funciona en cierto contexto puede ser inútil en otro similar.

Además de esto, aprendimos que es necesario reducir el tiempo en el que se obtiene retroalimentación por parte del cliente para actuar lo más rápido posible.

---

## **Motivación**

### **El mayor valor del mundo VUCA**

¿Cuál es el mayor valor para las organizaciones dentro de un mundo VUCA?

Rta.

Conocimiento. En un mundo VUCA donde las cosas cambian rápidamente, el principal valor es el conocimiento. Por ello, la principal habilidad es el aprendizaje.

### **Para saber más: Personal maps**

[Personal Maps](https://management30.com/blog/personal-maps-connecting-teams-improving-team-collaboration/ "Personal Maps") es una actividad donde podrás entender mejor a otra persona a través de un diagrama dibujando sus intereses como categorías. Este ejercicio es utilizado para impulsar la colaboración entre integrantes de un mismo equipo.

### **Confianza dentro del mundo corporativo**

Identifica algunas interacciones dentro de tu organización donde percibes una gran sensación de confianza entre ellos. ¿Qué es lo que logró ese nivel de confianza tan alto? Nota algo: ¿Hay autonomía en ese equipo? ¿Su propósito está claro? ¿Esas personas están evolucionando al adquirir nuevas habilidades? Ahora, intenta identificar ambientes en donde no hay ese nivel de confianza. ¿Qué consideras que podría mejorar?

Rta.

El primer paso para mejorar los niveles de confianza dentro de una organización es basarnos en 3 pilares: dar autonomía, tener propósito y volverse experto.

### **Lo que aprendimos**

Como lo decía Ricardo Semler: "Los miedos e inseguridades crean las estructuras de nuestras empresas". Para atacar este problema, podemos basarnos en los 3 pilares de la confianza:

- Dar autonomía: puedo hacer las cosas de la mejor manera que considero sea la mejor.
- Tener propósito: todo lo que hago tiene un claro propósito con el que estoy de acuerdo.
- Volverse experto: la empresa me da espacio para mejorar en lo que más gusta hacer.

---

## **Liderazgo**

### **Heteronomía**

¿Cuál es la definición de liderazgo situacional?

Rta.

El liderazgo situacional ocurre cuando emerge de acuerdo al contexto. En una situación dada, una persona puede ser líder; en un contexto dentro del mismo equipo, puede ser otra persona. Este liderazgo puede variar de acuerdo con las habilidades de la persona, dentro de ese momento y de otros factores.

### **Para saber más: Autonomía en Spotify**

Uno de los principales casos referencia de un ambiente ágil es Spotify, donde hay un trabajo sólido en el sentido de dar autonomía a sus colaboradores. Esto promueve situaciones de heteronomía y liderazgo situacional.

Este artículo en [Harvard Business Review](https://hbr.org/2017/02/how-spotify-balances-employee-autonomy-and-accountability "Harvard Business Review") muestra algunas de las características aplicadas en el contexto de Spotify.

### **Liderazgo en tu organización**

¿Has liderado algún proceso dentro de tu organización? En caso afirmativo, ¿cómo fue la toma de decisión para definirte como líder? ¿Este liderazgo surgió de forma natural dentro de un contexto debido a habilidades específicas que tenías para aquella situación o fue impuesto? Finalmente, ¿cuál fue el resultado final de ese liderazgo? ¿Podría haber sido mejor?

No hay una respuesta correcta o incorrecta al respecto. En ocasiones, el liderazgo situacional no es necesariamente la mejor opción para tu organización debido a su contexto. Sin embargo, reflexionar sobre ella ayuda a tener más claridad respecto a usarla (o no) como un posible camino dentro de tu equipo u organización.

### **Lo que aprendimos**

En un ambiente ágil, el liderazgo emerge naturalmente entre las personas, en paralelo a las habilidades mostradas para aquella posición y también dentro de ese momento del tiempo.

Además, el liderazgo es situacional; es decir, funciona en un contexto específico y no es fijo, por lo que puede cambiar conforme sea necesario.

---

## **Empoderar y delegar**

### **Los 7 niveles del Delegation Poker**

Delegation Poker es una herramienta de Management 3.0 que nos auxilia a mapear como una decisión es tomada en una determinada circunstancia dentro de la empresa. Se compone por 7 niveles cuyo orden es:

Rta.

1. Ordenar, 2) Vender, 3) Consultar, 4) Consensuar, 5) Aconsejar, 6) Informar y 7) Delegación completa

### **Mapeando el proceso de decisión**

Es muy probable que la toma de decisión sea hecha de forma diferente de acuerdo con la situación dentro de tu organización. Mapea 3 momentos en donde estuviste involucrado de alguna forma en algún proceso de toma de decisión recientemente.

Ahora, relaciona cada uno de esos 3 momentos con una categoría del Delegation Poker dentro de Management 3.0. Imagina que esa decisión es tomada a través de delegación completa. ¿Piensas que el resultado sería diferente? ¿Sería mejor o peor?

Nuevamente, no hay una respuesta correcta o incorrecta. Todo depende en gran parte de la cultura y del contexto de la organización. Algunas decisiones serán más autocráticas, otras podrán ser completamente delegadas. El secreto se basa justamente en encontrar el equilibrio para dar autonomía para que las personas puedan aprender y crecer, mientras cuidamos que el negocio no sea impactado por una decisión que tal vez esté fuera de lo que es realmente factible.

### **Lo que aprendimos**

Generalmente, las personas en un alto nivel jerárquico poseen un mayor conocimiento e información de todo el contexto; sin embargo, estos datos son más abstractos. Mientras tanto, en un nivel jerárquico más bajo hay muchos más detalles, pero la visión está un poco más limitada.

El secreto de un buen proceso de toma de decisión se basa justamente en encontrar el equilibrio respecto a ese conocimiento. Delegation Poker es una forma de mapear las decisiones y el nivel de empoderamiento dentro de las organizaciones.

---

## **Retroalimentación y conocimiento**

### **Retroalimentacion**

La retroalimentación requiere estar compuesta de:

Rta.

Contextualizar, Dar hechos, Valor de la mejora y Preguntas poderosas. Contextualizar muestra el escenario para la persona, mientras que la exposición deja clara la situación donde aquello ocurrió para que pueda recordar ese momento. El valor de la mejora deja visible lo que se gana al corregir aquel comportamiento. Finalmente, las preguntas poderosas buscan la reflexión sobre la situación.

### **Dando feedback positivo**

Algún colega tuyo debe haber hecho algo valioso en el trabajo durante los últimos días. Por más pequeño que haya sido, reconoce a esa persona. Acércate y dile algo como: "Disculpa, recordé que hiciste X. Creo que eso fue muy bueno porque eso ayuda mucho a nuestro equipo a ser/tener Y. ¡Felicidades y muchas gracias!"

¿Qué tal si comienzas una iniciativa con Thank you notes en tu área o incluso dentro de toda tu empresa? Pide que a lo largo de una semana las personas escriban los reconocimientos y los guarden en una caja. Luego, ábrela y muéstralos. Repite el proceso de forma recurrente. No olvides identificar quiénes dan retroalimentación y quiénes la están recibiendo.

[Puedes descargar los templates aquí (desde la web de Management 3.0)](https://management30.com/practice/kudo-cards/ "Puedes descargar los templates aquí (desde la web de Management 3.0)").

### **Lo que aprendimos**

La retroalimentación genera conocimiento y logra el crecimiento de la persona, haciendo que salga de una etapa de habilidad inconsciente y logrando que sea consciente de ella, con lo que será capaz de adquirirla.

Por ello, la retroalimentación debe ser llevada a cabo con cuidado: considerar el contexto, dar hechos, tomar en cuenta los valores y hacer preguntas poderosas requieren ser usados siempre dentro del proceso. Esta herramienta genera conocimiento para la organización.

---

## **Mejoras**

### **Kaizen y Kaikaku**

¿Qué alternativa describe las características y diferencias entre Kaizen Kaikaku?

Rta.

Kaizen involucra cambios pequeños realizados de forma incremental a lo largo del tiempo, mientras que Kaikaku requiere uno grande hecho una sola vez. Kaizen es un grupo de cambios pequeños e incrementales, mientras que Kaikaku generalmente representa una mudanza mayor que muchas veces viene de una decisión top-down.

### **Mejora continua de procesos**

Identifica un pequeño proceso de tu día a día que te genera incomodidad y crees que pueda ser optimizado de alguna manera. Puede ser algo dentro de tu vida profesional o personal. Cámbialo para que pueda ser mejorado.

Por ejemplo: si siempre olvido comprar pasta de dientes, puedo usar una app para tener una lista del supermercado donde esté incluida.

La mejora continua puede convertirse en un vicio. Parte de problemas grandes y piensa cómo podrías optimizarlos para mejorar performance, tener menos burocracia o reducir fallas.

### **Scrum y Kanban**

![Scrum](img/scrum.png "Scrum")

![Kanban](img/kanban.png "Kanban")
