# **Fundamentos de Agilidad - Primeros pasos para la transformación ágil**

## **El Metodo Ágil (agile)**

### **Origen del metodo agil**

Menciona las necesidades, durante el proceso de desarrollo, por las que nació el método ágil.

Rta.

Cambios en el mercado y nuevas regulaciones, ideas/hipótesis no probadas y generación de conocimiento.

---

## **Método Cascada (Waterfall)**

### **Caracteristicas de Waterfall**

Si un cliente pide cambiar el orden planeado en que las etapas serán ejecutadas, ¿es posible hacerlo?

Rta.

Nunca, la primera está orientada para disparar la segunda, y así sucesivamente. En Waterfall, cuando dividimos el modelo en etapas, la primera está orientada para disparar la segunda, y así sucesivamente. Con esto, se excluye la posibilidad de hacer algún cambio.

### **Lo que aprendimos en aula**

Es necesario que los requisitos/documentación sean aprobados y acordados con el cliente, así como por el responsable del proyecto. Una vez aprobado este documento, el flujo para lograr el objetivo no cambiará, tampoco su alcance.

En Waterfall, cuando dividimos el modelo en etapas, la primera está orientada para disparar la segunda, y así sucesivamente. Con esto, se excluye la posibilidad de hacer algún cambio.

---

## **Priorización**

### **Cómo priorizar**

¿Qué criterios debemos tener en cuenta para priorizar tareas?

Rta.

Su importancia, urgencia, el contexto de la empresa, el momento, el tipo de proyecto y el cliente.

### **Lo que aprendimos en aula**

Es la primera diferencia importante entre Agile y Waterfall. Pero, ¿qué es la priorización? ¿Cómo podemos definirla?

Es el proceso de determinar la tarea con mayor importancia y urgencia.

- Importancia: debe ser claro cuál es el objetivo que esa tarea ayudará a alcanzar.
- Urgencia: la actividad tiene un plazo corto de entrega.

El criterio para determinar la priorización de actividades depende del contexto de la empresa, del momento, del tipo de proyecto y del cliente. Sin embargo, independientemente de esto, generalmente debemos priorizar aquello que genera un mayor retorno financiero.

---

## **Flujo de ejecución**

### **Agile y Waterfall**

Aprendimos las 3 principales características que hacen distintos a ambos modelos y que traen como consecuencia mejores resultados. ¿Cuáles son?

Rta.

Aprendimos sobre priorizar tareas, cambios de flujo y retroalimentación durante la finalización de cada tarea.

### **Lo que aprendimos en aula**

Es necesario que la priorización sea hecha. Por ejemplo, la tarea "dormir" es más importante que la tarea "cocinar". Para hacerlo, requerimos los siguientes pasos:

- Paso 1 - El proyecto y el objetivo final deben ser establecidos para que el problema sea resuelto de manera rápida y eficaz.
- Paso 2 - Dividir en pequeñas metas. Determina la tarea que debe ser resuelta con mayor prioridad, conservando un flujo enfocado en lo que más importa.

---

## **Retroalimentación**

¿En qué momento se recomienda buscar retroalimentación de tu cliente?

Rta.

Al completar una etapa o tarea. Con esa entrada es posible hacer cambios en el flujo hasta eliminar tareas que ya no sean necesarias.

### **Lo que aprendimos en aula**

Es la tercera diferencia más relevante entre Agile y Waterfall. Son las evaluaciones y críticas sobre los resultados obtenidos, sean positivas o negativas, colaborando para el crecimiento de la empresa.

Sobre la metodología en general, aprendimos que:

- Primero, es necesario priorizar las tareas.
- Después de priorizar, se debe definir el plan/flujo de trabajo, para lograr dividirlo en etapas.
- Cuando se completa una etapa, es importante obtener retroalimentación de tu cliente.
- Con esa entrada (retroalimentación) es posible hacer cambios en el flujo hasta eliminar tareas que ya no sean necesarias.

---

## **¿ Que es ser ágil ?**

**¿Por qué es importante ser ágil?**

Rta.

Para entregar valor rápidamente, ofrecer entregas parciales, tener ciclos rápidos de entrega, retroalimentación constante y mejoras rápidas.

### **Lo que aprendimos en aula**

Ser ágil significa entregar valor rápidamente, tener un abordaje enfocado en lo que genera más valor; es decir, cuando algún cliente tiene un problema, debemos ofrecerle entregas parciales de acuerdo con sus necesidades. Estos ciclos rápidos de entrega hacen que la retroalimentación sea constante para mejorar el servicio/producto, alcanzando mejores resultados al final del proceso.

---

## **Manifiesto ágil**

### **La relevancia del Manifiesto Ágil**

Elige cuáles son valores básicos del Manifiesto.

Rta.

Interacciones > procesos, funcionalidades > documentación, colaboración > contrato, adaptación > seguir un plan.

### **Lo que aprendimos en aula**

Fue escrito en 2001. Su objetivo era descubrir cuáles eran las semejanzas entre los diversos procesos de desarrollo de software basado en la experiencia de quienes lo escribieron, y qué logra que los clientes queden satisfechos. A partir de ahí llegaron a 4 valores:

1. Las interacciones entre individuos son más importantes que los procesos y herramientas; es decir, la manera como un equipo trabaja impacta directamente el . desarrollo de lo que será producido.
2. Tener software funcionando es más importante que documentación detallada.
3. La colaboración del cliente es más importante que la negociación del contrato; es decir, la proximidad es esencial para que la retroalimentación tenga mayor valor y lograr que eso impacte el flujo de trabajo.
4. Responder a los cambios es más importante que seguir un plan.

---

## **Cases**

### **Agile en tu compañía**

Implementar prácticas de agilidad de la misma forma en que fueron aplicadas en otras organizaciones garantiza siempre buenos resultados. ¿Qué opción describe mejor si esto funciona o no?

Rta.

Debe analizarse el contexto, qué se buscaba resolver y los factores dentro de cada compañía antes de implantarla en tu equipo. ¡Correcto! Que hayan generado un efecto positivo no implica que funcionará en todas.

### **Lo que aprendimos en aula**

Con el uso de métodos ágiles a partir de cases, logramos observar diversos resultados dentro de pequeñas y grandes empresas. Esto no implica un simple copy y paste de prácticas de agilidad que fueron tomadas de otras organizaciones: el que hayan generado un buen resultado no implica que la fórmula funcionará en todas. Se debe analizar y entender por qué determinada empresa aplicó cierta característica ágil, qué buscaba resolver con ello y analizar los factores dentro de cada empresa antes de implantar la misma metodología en tu equipo.
