# **Organización de equipos ágiles - Las funciones existentes en un equipo**

## **El equipo ágil**

### **Formando un nuevo equipo**

¿Tu empresa tiene proyectos ágiles? Si es así, intente identificar el papel de cada persona en uno de los proyectos existentes. ¿Están todas estas personas involucradas en el proyecto de alguna manera en todo momento? En otras palabras, ¿están siempre validando y adaptando la ruta de ese proyecto?

Ahora bien, si tu empresa no cuenta con equipos ágiles, sino microequipos que tienen conocimiento de partes aisladas de un producto o proyecto, ¿cómo podrías re-organizar este equipo para que sea más eficiente, reducir cuellos de botella y aún más, asegurando que todas las partes involucradas estén alineadas sobre el proyecto todo el tiempo?

En un equipo ágil, todo el conocimiento necesario para que el proyecto evolucione está dentro del mismo equipo. Esto significa tener un equipo multidisciplinario, que puede involucrar en el mismo equipo: vendedores, marketing, desarrolladores, infraestructura, RRHH, etc. Por supuesto, cada una de estas personas y áreas tendrá un rol diferente en el proyecto y en el futuro aprenderemos qué roles pueden existir.

Pero el punto principal es: todo lo que el proyecto necesita en conocimiento para evolucionar, debe estar dentro del mismo equipo.

### **Equipos Ágiles**

¿Qué alternativa define mejor un equipo ágil?

Un equipo ágil es un grupo de personas con todos los conocimientos necesarios para producir algo. Exactamente. En un equipo ágil, los conocimientos necesarios para cumplir los objetivos centrales del proyecto están contenidos en el equipo multidisciplinar.

### **Lo que aprendimos**

En esta clase aprendimos sobre la diferencia entre Modelo Waterfall (Cascada) y Agile.

Modelo en cascada: favorece los problemas de comunicación (el famoso teléfono inalámbrico), con la necesidad de muchos documentos y protocolos, muchos traspasos entre equipos y, con ello, la validación de todo lo que se construyó durante el proceso de desarrollo de un proyecto solo ocurre al final.

Por otro lado, existe un enfoque muy fuerte en la especialización de las personas.

Modelo ágil: en un equipo ágil, todas las personas que participarán en el desarrollo del proyecto confluyen en todas las etapas de este proceso, siendo un solo equipo.

Se centra en la inspección y adaptación de productos, la transparencia y la eficiencia.

---

## **La autoorganización**

### **Autoorganización**

¿Cuáles son los 3 elementos mínimos necesarios para que un equipo pueda ser auto organizable?

Rta.

Objetivos compartidos, timebox y restricciones. Exactamente, sabiendo para donde el equipo necesita ir (los objetivos), hasta cuando pueden llegar (timebox), y cuáles son los límites que tienen (restricciones), el equipo podrá auto-organizarse para conseguir alcanzar su objetivo!

### **Auto-Organización en la práctica**

La auto-organización es justamente lo opuesto al mando y control. Mapee la última vez que participó en un proyecto, en el cual el entorno se gestionó a través de comando y control e intente adaptarlo a un entorno de organización de equipo ágil, donde las cosas se auto-organizan.
Auto-Organización en la práctica
Próxima Actividad

La auto-organización es justamente lo opuesto al mando y control. Mapee la última vez que participó en un proyecto, en el cual el entorno se gestionó a través de comando y control e intente adaptarlo a un entorno de organización de equipo ágil, donde las cosas se auto-organizan.

Uno de los secretos para poder hacer esta adaptación es pensar qué objetivos se necesitaban alcanzar, en cuánto tiempo (timebox) y qué restricciones implican.

Por ejemplo: se le asignó la tarea de organizar la fiesta de vacaciones de su empresa para 150 personas. En este caso, tendríamos los 3 elementos:

- El objetivo: organizar la fiesta de la empresa.
- Timebox: debe suceder en Noviembre.
- Restricciones: El presupuesto es de $30.00 por persona. Cada colaborador solo puede llevar a un invitado. La fiesta tiene que ser durante la tarde. Tiene que suceder en la ciudad de la empresa, para facilitar los desplazamientos.

Con esta información, el propio equipo se organizará para lograr el objetivo determinado.

---

## **Los tipos de equipos ágil**

### **Los tipos de equipos ágiles**

De acuerdo con los 3 tipos de equipo que actúan en ambientes ágiles, tenemos:

- El equipo que se enfoca en maximizar el valor para la entrega de un feature.
- El equipo que se enfoca en grupos que poseen comportamientos similares.
- El equipo que tiene foco en optimizar apenas un elemento.

Señale la alternativa que definen estas 3 definiciones

Rta.

Funcionalidad, Jornada del Cliente, Componente.

### **Equipos en ambientes ágiles**

Existen tres tipos de equipos que están presentes en los ambientes ágiles:

Equipos de funcionalidad, componente y jornada del cliente. Intenta identificar algún equipo en el que hayas trabajado que se encaje con alguno de esos 3 tipos de equipos ágiles.

Observe e identifique, en función de las características, qué tipo de equipo es el que más encaja.

- Funcionalidad: Equipo que se enfoca en maximizar el valor en la entrega de un feature.
- Jornada del cliente: Grupos con comportamientos similares.
- Componente: Equipo que tiene como foco optimizar un solo elemento.

### **Lo que aprendimos**

En esta clase aprendimos 3 principales tipos de equipos ágiles que se auto-organizan:

- Funcionalidades: Ese equipo se organiza en torno de una funcionalidad y se preocupa en maximizar el valor en la entrega al usuario para una mejor experiencia posible. El equipo necesita ser multidisciplinario, ya que necesita trabajar en diversos puntos en el código.
- Componente: este equipo funciona muy bien para hacer optimizaciones locales; sin embargo, puede ser crítico alterar un componente, ya que puede ser usado en diversas funcionalidades, además de garantizar la sincronización de los componentes que fueron alterados.
- Jornada del cliente: este equipo se enfoca en grupos de clientes que poseen comportamiento o necesidades semejantes.

---

## **El equipo Scrum**

### **El scrum team**

De acuerdo con lo que aprendimos en esta clase, existen 3 papeles importantes que componen el scrum team, ¿Cuáles son?

Rta.

Eficacia, Eficiencia y Calidad. Exactamente, esos 3 papeles (Eficacia , son los dueños del producto; Calidad, son los que se encargan de la calidad y Eficiencia el papel de nuestro scrum master) juntos componen el Scrum Team y vamos a entenderlo más a fondo en las próximas clases.

### **Organizando equipos**

Piensa en ti mismo, dentro de tu actuación o rol en las organizaciones, ¿qué tipo de conocimiento y rol desarrollaste en los proyectos en los que participaste? ¿Roles más relacionados con el negocio? ¿Roles más relacionados con la tecnología y la calidad de lo que se entregó? ¿Y / o roles más vinculados a asegurar la eficiencia?

Esta identificación te ayudará a entender en las próximas clases si tuviste algún tipo de desempeño relacionado con un equipo ágil y también cuál es el nombre de este rol dentro del equipo.

### **Lo que aprendimos**

Aprendimos que el equipo de Scrum es un grupo de personas que tienen como objetivo hacer productos excepcionales, y para que eso suceda, son necesarias 3 responsabilidades:

- Eficaz (Product Owner): responsable de asegurar que el producto a desarrollar tenga sentido para el mercado. De nada sirve producir el mejor producto si no tienes un cliente para él.
- Calidad (Dev. Team): responsable de asegurar que el producto elaborado sea el correcto para ese mercado específico y público objetivo.
- Eficiencia (Scrum Master): responsable de asegurar que el equipo opere de la mejor manera posible.

Es decir, en esta clase aprendimos que un equipo ágil se organiza de manera que cuenta con personas que se encargan de asegurar que el producto a desarrollar esté alineado con lo que espera el mercado, que la calidad y tecnologías empleadas estén siendo adecuadas y además por personas responsables de asegurar que el equipo funcione de manera eficiente, sin cuellos de botella.

---

## **El Product Owner**

### **El rol del Product Owner**

En esta clase aprendimos qué hace un Product Owner dentro de una organización y su importancia en el desarrollo de un proyecto.

¿Cuál es el objetivo de la persona que tiene el rol de Product Owner en un proyecto?

Rta.

Encontrar formas de maximizar el valor que ese producto genera para la empresa. Para eso, esta persona necesita algunas habilidades como sentido de propiedad, enfoque y conocimiento profundo del negocio.

### **¿Qué es el Product Owner en el marco de Scrum y cuáles son sus áreas de atención?**

Vamos a leer este [artículo](https://medium.com/@kleer.la/qu%C3%A9-es-el-product-owner-en-el-marco-de-scrum-y-cu%C3%A1les-son-sus-%C3%A1reas-de-atenci%C3%B3n-2bddbb232775#:~:text=Ser%20Product%20Owner%20implica%20mantener,impactan%20en%20los%20resultados%20deseados "artículo") juntos que nos ayudará a profundizar más sobre las áreas de actuación del Product Owner y nos ayudará a entender porque es el más estratégico en organizaciones que desarrollan productos (o servicios) con Scrum.

### **Lo que aprendimos**

¿Qué es Product Owner?

En esta clase aprendimos que el responsable del rol de Product Owner tiene el desafío de maximizar el valor que ese producto generará para el negocio.

Para ello, el PO, además de tener un alto enfoque y sentido de pertenencia, deberá dialogar frecuentemente con el resto de áreas que forman parte de la empresa, para entender cuáles son los puntos clave frente a los dolores de los clientes y conocer a profundidad el producto y mercado en el que opera la empresa.

¿Cómo es la vida diaria de un PO?

En su vida diaria, el PO tendrá que describir lo que se debe hacer y para eso es necesario hablar con otras áreas para comprender las necesidades y también lo que se puede o no se puede hacer. Además de priorizar lo que se necesita construir, el PO a menudo tendrá que decir no a las características que agregan menos valor al negocio o proyecto en ese momento.

---

## **El Development Team**

### **El rol del development team**

Según lo aprendido en esta clase, un Equipo de Desarrollo está compuesto por personas que pueden ser:

- Especialista: alguien con profundos conocimientos sobre cualquier tema.
- Generalista: una persona que sabe un poco de todo, pero no tiene un contenido profundo.
- En forma de T: alguien con experiencia en algo, pero con conocimientos generales en otras cosas.

Ya conocemos los tipos de profesionales del Equipo de Desarrollo, pero ¿cuál es el papel de un equipo de desarrollo (development team) en un equipo ágil?

Rta.

Construir, validar e integrar el incremento del producto. Exactamente, un equipo de desarrollo es el responsable de ejecutar lo que fue definido, validando para garantizar que está adecuado.

### **Development Team fuera de tecnología**

Es común en el mercado de áreas no tecnológicas que utilizan métodos ágiles. Más aún, en estos momentos cuando las empresas están pasando por un proceso de transformación ágil, introduciendo agilidad empresarial en el entorno corporativo.

Piense en escenarios en los que un equipo de desarrollo no es un equipo de tecnología. ¿Qué perfiles existirían dentro de este equipo? ¿Cómo sería la construcción, validación e integración de este "producto"?

Considere la creación de una nueva área de ventas en su empresa y se ha planificado la contratación de 12 personas nuevas para esta área entre coordinación, ventas y preventa.

El Product Owner identificó todas las necesidades, conoció los productos con los que trabajarán estas personas y mapeó las características que se buscarán en estas personas.

El equipo que realizará el trabajo, es decir, el Equipo de Desarrollo, hará las entrevistas, negociación salarial, integración y evaluación de desempeño de personas especializadas en diferentes temas, como procesos de selección, ventas, liderazgo, psicología y negociación, para hacer parte de un solo equipo: en este caso, el equipo de "reclutamiento de nuevo equipo de ventas".

### **Lo que aprendimos**

Aprendimos que un equipo de desarrollo es responsable de construir, validar e integrar un incremento de producto.

Además, este equipo está compuesto por personas que no tienen títulos, todos son "miembros" de ese equipo.

Y una de las características más interesantes para que una persona sea parte de un Equipo de Desarrollo o incluso de un equipo ágil es que tiene forma de T, es decir, además de tener al menos una experiencia en la que es especialista, también logra ser generalistas, pudiendo actuar con soltura en diferentes roles y temas.

---

## **El Scrum Master**

### **Características del Scrum Master**

Además de la habilidad que tiene un Scrum Master de ser un Líder en Servicio, cuyo principal objetivo es ayudar a los demás para que logren el resultado deseado, un Scrum Master tiene muchas otras capacidades.

¿Cuál de estas características NO representa a una persona con el rol de Scrum Master?

Rta.

Priorizar lo que será hecho. Este es un papel del Product Owner y no del scrum master.

### **Preguntas para reflexionar**

Es posible que hayas notado que durante este curso y cursos similares, siempre existen estas preguntas cuyo propósito es pedirte que reflexiones sobre un tema determinado. Ya sea para ponerse en una situación, imaginar cómo resolvería cierto problema, etc.

El objetivo es que siempre explores nuevas alternativas pensando con tus propios conocimientos y poniéndolos en práctica. No hay correcto o incorrecto en este caso.

Este es precisamente el rol del coach y es una de las tareas del Scrum Master. Hacer preguntas al equipo que genere reflexión y asegurarse de que las personas puedan dar sus propias respuestas.

Intente practicar esto con sus compañeros de trabajo.

Para aquellos que no están acostumbrados, es difícil adaptarse a no dar respuestas sino a hacer preguntas para ayudar a las personas a reflexionar y encontrar las respuestas. Pero este es un proceso de engrandecimiento.

Enseñarás a otras personas el proceso de resolución de un problema, con ellos mismos llegando a la conclusión y rompiéndolo también pueden aprender muchas cosas nuevas mientras se generan estas reflexiones con las personas.

### **Lo que aprendimos**

Aprendimos que la persona en el rol de Scrum Master es responsable de maximizar la eficiencia del Scrum Team, promover y apoyar Scrum y ayudar a mejorar las interacciones existentes.

Esta persona tiene algunas características principales:

- Líder Servidor: su principal objetivo es hacer que otras personas tengan éxito.
- Coach: persona que puede hacer preguntas que generen reflexión.
- Facilitador: puede organizar el trabajo.
- Maestro: puede enseñar para el equipo.
- Elimina impedimentos.
- Experto en Scrum.
- Tiene Habilidades Sociales: capacidad para hablar e interactuar con otras personas.
- Agente de cambio: tiene como objetivo mejorar la organización, no solo el equipo scrum.

---

## **El Equipo Kanban**

### **Yendo a fondo en Kanban**

Kanban es un tema extremadamente rico dentro de agile, y una forma poderosa de visualizar y limitar el trabajo que está siendo hecho por un equipo.

[Aquí](https://marvin-soto.medium.com/la-metodolog%C3%ADa-kanban-6ab002502831 "Aqui") les dejo un artículo que los ayudará a conocer y profundizar más sobre Kanban! Estén atentos a las redes de Alura Latam por si traemos novedades con cursos que los ayuden a seguir profundizando!

### **Profundizando Scrum**

Scrum además de tener roles definidos y características marcadas para los equipos, nos define también algunos artefactos para ayudarnos en nuestro dia a dia con scrum.

Les dejo aqui un [link](https://medium.com/@andrewdjandrw/qu%C3%A9-es-scrum-674c6b791af4 "link") donde podrán repasar, qué es scrum, sus roles y conocer sobre sus artefactos.

### **Lo que aprendimos**

Kanban es una evolución de cómo organizar el trabajo de un Scrum Team, sin tener algunos de los roles obligatorios de Scrum y con un fuerte enfoque en garantizar la capacidad de trabajo.

El método Kanban consta de tareas y se divide en 5 etapas: documentación, creación de prototipos, codificación, pruebas y entrega.

![Kanban](img/kanban.png "kanban")

Se usan post-its en cada uno de los pasos, que generalmente contienen una breve explicación de la tarea. Un determinado grupo de personas trabaja en cada etapa, que puede ser una, dos o incluso más.

Tenemos los roles opcionales dentro de la herramienta Kanban que son:

- Service Delivery Manager: es la persona que vela por que los artículos no se detengan, tiene el rol de facilitador en el flujo de cambios y mejoras.
- Service Request Manager: su función principal es ordenar y colocar los artículos, qué artículo tendrá el mayor impacto y retorno de la inversión.
