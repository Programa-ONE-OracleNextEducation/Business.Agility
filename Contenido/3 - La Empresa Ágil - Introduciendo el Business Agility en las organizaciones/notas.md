# **La Empresa Ágil - Introduciendo el Business Agility en las organizaciones**

## **Business Agility**

### **Diseñemos la Agilidad empresarial**

Piensa en el contexto de la empresa en la que trabajas. Diseña mentalmente o en una hoja de papel o en un documento de texto al menos 5 áreas existentes. Comienza a conectar estas áreas usando flechas direccionales que identifiquen cómo se relacionan estas áreas, como, por ejemplo:

En este caso, el área de ventas depende del área de recursos humanos cuando necesita contratar a una nueva persona. Intente pensar en al menos 5 interacciones entre las áreas.

Ahora con estas áreas y sus interacciones mapeadas, piense si este proceso que integra estas áreas (por ejemplo, el proceso de contratación de vendedores) es burocrático o no y cuando encuentres un proceso burocrático, piense cómo podría agilizar este proceso, sin perder los controles necesarios, pero eliminando burocracias innecesarias.

El objetivo de Business Agility es precisamente eliminar estas barreras burocráticas entre áreas, haciendo más ágil la organización.

### **Propósito del Business Agility**

¿Cuál es el propósito final de Business Agility?

Rta.

Hacer que la “Respuesta al cambio” se torne un diferencial competitivo de la empresa. A partir del momento en que es construido ese diferencial competitivo de “Respuesta al cambio”, la empresa tiene la posibilidad de adaptarse a cualquier nueva estrategia que aparezca. Y como vivimos en un ambiente donde todo cambia muy rápidamente, ese diferencial competitivo pasa a ser fundamental para el éxito de la organización.

### **Lo que aprendimos**

En esta clase, aprendimos qué es la agilidad empresarial y cómo mapear las burocracias entre áreas y eliminarlas puede generar ganancias para el negocio.

Al eliminar burocracias innecesarias en los procesos, es posible hacer que una organización sea más adaptable a los cambios, porque cuando un cambio es necesario, será menos traumático, ya que habrá menos barreras para el cambio.

Cuando hablamos de agilidad, eso es exactamente lo que queremos, una mejor adaptabilidad a los cambios y no, como las malas interpretaciones que ocurren, hacen que un entorno sea más rápido.

---

## **Identificando la complejidad**

### **Problemas subjetivos**

Cuando decimos que tenemos un problema o proceso subjetivo, estamos diciendo que:

Rta.

Este problema tiene diferentes formas de solucionarse, pero solo estaremos seguros de si la forma era correcta después de probar la solución. Los problemas subjetivos también terminan trayendo mayor complejidad.

### **Identificando complejidades**

Dentro de tu área, identifique cuán perecedera es la información. Para ello, piensa e identifica 3 procesos existentes dentro de tu área; principalmente, procesos que integran tu área con otras áreas.

Una vez mapeados estos procesos, identifica si se trata de un proceso Intelectivo (con solo una forma de hacerlo bien) o Subjetivo (existen varias formas correctas de realizar este proceso y sólo puedes saber si se utilizó la forma correcta después de hacerlo).

Cuantos más procesos subjetivos tengas, más complejo será tu entorno. Cuantos más problemas intelectuales mapee, menos complejo será tu entorno.

### **Lo que aprendimos**

En esta clase aprendimos a identificar la complejidad de los problemas y procesos de un área y una organización.

Los problemas se dividen básicamente en dos grupos: Intelectivo o Subjetivo.

Los problemas subjetivos son aquellos que tienen más de una forma de resolverse y solo sabemos si la forma utilizada fue la más correcta después de intentar resolverlo. Cuantos más problemas subjetivos tengamos, más complejo podemos decir que es el entorno que estamos mapeando.

Por otro lado, un problema intelectual es aquel en el que identificamos una única forma posible de resolverlo y esa forma ya está probada y comprobada. Esto hace que un entorno que tiene más procesos y problemas intelectuales sea menos complejo.

---

## **Estructura organizacional**

### **Estructura Jerárquica**

¿Qué se puede considerar un punto positivo de una estructura fuertemente jerárquica?

Rta.

Existe una tendencia a tener un mayor control sobre todo lo que se hace. En un entorno altamente jerárquico, se tiende a tener muchos niveles para tomar una decisión (ya sea importante o no) generando un mayor control sobre lo que se hace. Por otro lado, esto también tiende a generar una mayor lentitud.

### **La Mejor Estructura Organizacional**

¿Cuál es la mejor estructura organizacional para un entorno ágil?

Rta.

Ninguna, ya que es posible tener un entorno ágil en cualquiera de las estructuras organizativas. Lo que va a determinar si una empresa es ágil o no es su capacidad de reaccionar ante los cambios y para esto puede tener cualquier estructura. Cada una de ellas tiene ventajas y desventajas, es decir, ninguna es perfecta y tiene sus efectos secundarios.

Elegir qué metodología utilizar en su empresa requiere un estudio y análisis cuidadoso de los impactos en las personas y la cultura organizacional.

### **Reflexione sobre su estructura**

Piensa en la estructura organizacional de tu empresa hoy y trata de encajarla en otra estructura que te haya parecido interesante durante la clase.

Por ejemplo: ¿tu empresa es muy jerárquica? ¿Cómo reaccionarían las personas con las que trabajas al trabajar en un ambiente holocrático? ¿Crees que los procesos funcionarían o necesitarían más control?

¿Tu empresa es holocrática? Entonces, ¿cómo reaccionarían las personas al trabajar en un ambiente altamente jerárquico? ¿Los procesos estarían mejor o peor organizados?

Realizar este análisis es importante para darnos una mayor claridad a la hora de elegir cambiar una estructura organizacional. Recuerde siempre que afectará la forma en que trabajan varias personas y no todos se sentirán cómodos. Independientemente, siempre tenga en cuenta el resultado para el negocio que este cambio puede generar, ya sea que ese resultado sea positivo o negativo.

### **Lo que aprendimos**

En esta clase aprendimos cómo los diferentes tipos de estructuras organizacionales pueden influir en un entorno de agilidad y transformación ágil.

También aprendimos que independientemente de la estructura organizacional, ya sea en un modelo más jerárquico, incluso en un modelo más holocrático, es posible ser ágil. Es suficiente que las interacciones entre las áreas sean ligeras y no afecten la capacidad de adaptación de la organización a los cambios.

---

## **El Rayo X de la empresa agil**

### **Construyendo un escenario más ágil**

Ya debes tener mapeado, sea mentalmente o en un papel o documento electrónico, qué áreas y puntos de interacción hay entre las áreas de tu empresa. Si no es así, imagina que estas áreas trabajan como silos completamente separadas y con una fuerte barrera de integración entre diferentes áreas.

Ahora rompamos estos silos. Dibuja una estructura donde estas áreas se integren. Ya sea co-creando procesos nuevos o existentes o mediante equipos multidisciplinares. ¿Cómo se vería esta estructura en tu organización? ¿Cuáles crees que serían los beneficios? ¿Y los puntos negativos?

Piensa también en cuáles serían las resistencias que sufriría este enfoque. ¿Serían muchas? ¿Pocas?

Romper los silos existentes entre las áreas es esencial para construir una empresa más ágil que pueda responder mejor a los cambios.

Sin embargo, la forma de romper estos silos es una decisión compleja que involucra a muchos actores dentro del entorno corporativo. La co-creación de un proceso más ágil es un paso fundamental para crear conciencia en todas las áreas involucradas y para lograr una aceptación más fácil y el compromiso se puede crear a través de metas compartidas y el objetivo final de una reducción de la burocracia, con todas las ganancias que esto puede traer.

Estructuras organizacionales más avanzadas, como squads o toda una organización multidisciplinar enfocada en generar valor, son pasos que también se pueden dar, pero requieren un buen nivel de madurez y también tienen sus riesgos.

### **Reduciendo Silos**

El primer paso posible para reducir los silos organizacionales es a través de la co-creación de procesos entre las áreas que dependen unas de las otras. Para orientar un buen resultado para el negocio y facilitar este camino, es importante hacer:

Rta.

Simplificar los procesos y tener una meta compartida entre las áreas, lo que hará más evidente el compromiso entre estas áreas. El enfoque de Business Agility es reducir las burocracias, pero para romper los silos entre áreas, las áreas involucradas deben estar verdaderamente comprometidas. Esto se puede hacer precisamente simplificando los procesos que reducirán la burocracia y harán que ambas áreas compartan metas de resultados y se les cobre por esas metas.

### **Lo que aprendimos**

Cuando buscamos un entorno más ágil para nuestra corporación, uno de los principales desafíos que encontramos es reducir los silos existentes entre las áreas.

Los silos son aquellas áreas que operan de forma independiente entre sí y que muchas veces generan incluso desalineación en el negocio (una compitiendo y no colaborando con la otra).

Aprendimos que para reducir estos silos, necesitamos hacer que estas áreas comiencen a integrar sus procesos co-creándolos y generando un compromiso entre ellos, reduciendo burocracias y mediante metas compartidas.

Finalmente, incluso es posible romper estos silos creando estructuras multidisciplinares, que simplifican las burocracias, hacen que las áreas sean más conocedoras y, en consecuencia, más ágiles.

---

## **Enfoque en la generacion de valores**

### **La Menor Unidad de Valor**

¿Cuál definición describe mejor el concepto de la menor unidad de valor?

Rta.

Un conjunto de funciones y actividades, que agrupadas son mínimamente suficientes para que una determinada operación pueda ser realizada generando valor para el negocio. Cuando encontramos la menor unidad de valor, tenemos mapeado exactamente todo lo que es necesario para que aquella operación, sea la creación de un producto nuevo o de una nueva unidad de negocio, mínimamente pueda existir y genera valor para el negocio.

### **Encontrando tus unidades de valor**

En tu empresa , ¿qué producto o iniciativa se está creando o perfeccionando ahora? ¿Participas en ello?

Si es así, piensa en tu papel dentro del contexto de todo ese producto o iniciativa. ¿Tu función por sí sola genera valor para el negocio? Toma cuidado, para recordar qué trabajos pueden haber sido necesarios realizar antes o después de que este tu papel entrara en escena.

Mapea la cadena de dependencias de este producto o iniciativa e intenta identificar exactamente cuál es la menor unidad de valor para esa iniciativa.

Al encontrar la menor unidad de valor, tendrás una claridad de en lo que es necesario enfocarse para que ese producto genere un resultado para el negocio. Lo más importante es que tendrás una idea clara de qué posibles "silos" están involucrados, para que puedas hacer que estos diferentes roles o áreas se integren de manera ágil.

### **Lo que aprendimos**

En esta clase aprendimos que las empresas ágiles tienen un enfoque muy fuerte en generar valor para el negocio.

Una forma de mapear dentro de la organización cuáles son las unidades que generan valor es identificar las unidades de valor más pequeñas, es decir, un grupo mínimo de funciones e iniciativas que trabajando en conjunto pueden hacer que un producto o iniciativa genere un resultado para el negocio.

Este mapeo es importante para encontrar posibles silos existentes y mapear qué puntos de integración entre áreas pueden causar cuellos de botella que impiden que una empresa sea realmente ágil.

---

## **Como una empresa aprende a ser agil**

### **Proceso para tener una empresa ágil**

En el orden correcto, ¿qué pasos implica el proceso para que una empresa sea ágil?

Rta.

Entender, Problematizar, Investigar, Conectar, Practicar, Compartir (y repetir). Estos 6 pasos que ocurren repetidamente comprenden el proceso de avanzar hacia una empresa ágil.

### **¿Cómo tu empresa puede ser ágil?**

Reflexiona sobre tu organización. ¿Cómo la calificarías en términos de agilidad?

Si crees que está muy rezagada y es muy burocrática y enyesada, ¿cómo imaginas que podrías mejorar este escenario? ¿Qué problemas ves que podrían mejorarse y optimizarse? Además, ¿qué crees que necesitarías estudiar, conectar y practicar para poder resolver este problema?

Si ya consideras que tu empresa ya es una organización ágil, ¿será que no hay puntos para mejorar? Intenta mapear el paisaje y tu entorno.

Ahora que has realizado este ejercicio, retrocedamos un paso. Piensa si estas soluciones que se te ocurrieron están realmente relacionadas con la realidad de su empresa. ¿Será que diste un paso muy largo? ¿O demasiado corto?

Cuando hablamos de un proceso de transformación ágil, no hay recetas. No hay procesos mágicos que solucionen nuestro problema. Todo debe contextualizarse dentro de la realidad de la empresa y pensando en lo que es factible para este entorno.

### **Lo que aprendimos**

En esta clase aprendiste que la forma en que una organización sea ágil dependerá mucho de su contexto.

Precisamente por eso, lo primero que debes hacer es pensar en lo que significa ser ágil para tu empresa.

A partir de ahí, problematizarás, es decir, identificarás los problemas que se pueden resolver en tu organización, y luego buscarás soluciones a estos problemas, conectando estas investigaciones con tu contexto y el mundo real.

Una vez conectado al mundo real, pon en práctica las soluciones. Valida hipótesis, prueba, mira los resultados. Finalmente, comparte estas pruebas y resultados con otros miembros de la organización. Y repite todo este proceso indefinidamente.
